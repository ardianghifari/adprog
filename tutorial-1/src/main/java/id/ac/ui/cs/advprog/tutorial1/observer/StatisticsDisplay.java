package id.ac.ui.cs.advprog.tutorial1.observer;

import java.util.Observable;
import java.util.Observer;
import java.lang.Math;

public class StatisticsDisplay implements Observer, DisplayElement {

    private float maxTemp = 0.0f;
    private float minTemp = 200;
    private float tempSum = 0.0f;
    private int numReadings;

    public StatisticsDisplay(Observable observable) {
        // TODO Complete me!
        observable.addObserver(this);
    }

    @Override
    public void display() {
        System.out.println("Avg/Max/Min temperature = " + (tempSum / numReadings)
                + "/" + maxTemp + "/" + minTemp);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            WeatherData w = (WeatherData) o;
            tempSum += w.getTemperature();
            numReadings += 1;
            if (numReadings == 1) {
                maxTemp = tempSum;
                minTemp = tempSum;
            }
            else {
                maxTemp = Math.max(maxTemp, Math.max(tempSum, minTemp));
                minTemp = Math.min(minTemp, Math.min(tempSum, maxTemp));
            }
            display();
        }
    }
}
